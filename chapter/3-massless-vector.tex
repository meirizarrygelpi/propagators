% Copyright (c) 2015 Melvin Eloy Irizarry-Gelpí
\chapter{Maxwell Vector}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The action functional for a Maxwell vector wave $A$ is given by
\begin{equation}
	S[A] = \frac{1}{8 e^{2}} \int \mathrm{d}x \left[ F^{ab}F_{ab} + \frac{2}{\xi} (\partial \cdot A)^{2} \right], \qquad F_{ab} = \partial_{a}A_{b} - \partial_{b}A_{a}.
\end{equation}
Here the second term is a gauge-fixing term. Write
\begin{equation}
	(\partial \cdot A)^{2} = \eta^{ab}\eta^{cd} (\partial_{a}A_{b})(\partial_{c}A_{d}),
\end{equation}
and use the product rule
\begin{equation}
	(\partial_{a}A_{b})(\partial_{c}A_{d}) = \partial_{a} (A_{b} \partial_{c} A_{d}) - A_{b} (\partial_{a} \partial_{c}) A_{d},
\end{equation}
to write
\begin{equation}
	(\partial \cdot A)^{2} = \eta^{ab} \eta^{cd} \partial_{a} (A_{b} \partial_{c} A_{d}) - A_{a} \eta^{ac} \partial_{c} \partial_{d} \eta^{db} A_{b}.
\end{equation}
You also have
\begin{equation}
	F^{ab}F_{ab} = \eta^{ac} \eta^{bd} F_{ab}F_{cd} = \eta^{ac} \eta^{bd} (\partial_{a}A_{b} - \partial_{b}A_{a})(\partial_{c}A_{d} - \partial_{d}A_{c}),
\end{equation}
which leads to
\begin{equation}
	F^{ab}F_{ab} = 2\eta^{ac} \eta^{bd} ((\partial_{a}A_{b})(\partial_{c}A_{d}) - (\partial_{a}A_{b})(\partial_{d}A_{c})).
\end{equation}
The first term has both derivatives contracted with a metric tensor; the last term has each derivative contracted with a field. Use the product rule again to write
\begin{equation}
	\eta^{ac} \eta^{bd} (\partial_{a}A_{b})(\partial_{c}A_{d}) = \eta^{ac} \eta^{bd} \partial_{a} (A_{b} \partial_{c} A_{d}) - A_{a} \eta^{ab} (\eta^{cd}\partial_{c} \partial_{d}) A_{b},
\end{equation}
and
\begin{equation}
	\eta^{ac} \eta^{bd} (\partial_{a}A_{b})(\partial_{d}A_{c}) = \eta^{ac} \eta^{bd} \partial_{a} (A_{b} \partial_{d} A_{c}) - A_{a} \eta^{ac} \partial_{c}\partial_{d} \eta^{db} A_{b}.
\end{equation}
The action becomes
\begin{equation}
	S[A] = \frac{1}{2} \int \mathrm{d}x \left[ A_{a} \left(-\frac{1}{2 e^{2}}\eta^{ab}\eta^{cd}\partial_{c} \partial_{d} + \frac{1}{2e^{2}} \left[1 - \frac{1}{\xi} \right] \eta^{ac} \partial_{c}\partial_{d} \eta^{db} \right) A_{b} \right].
\end{equation}
Here I have ignored total derivative terms that lead to boundary integrals. From here you can read the kinetic operator
\begin{equation}
	K^{ab}(x|y) = \delta(x - y) \left(-\frac{1}{2e^{2}}\eta^{ab}\eta^{cd}\partial_{c} \partial_{d} + \frac{1}{2e^{2}} \left[1 - \frac{1}{\xi} \right] \eta^{ac} \partial_{c}\partial_{d} \eta^{db} \right)
\end{equation}
The Green function $G_{ab}$ satisfies
\begin{equation}
	\int \mathrm{d}z \left[ K^{ac}(x|z) G_{cb}(z|y) \right] = \delta_{b}^{a} \delta(x - y),
\end{equation}
which translates to a partial differential equation:
\begin{equation}
	\left(-\frac{1}{2e^{2}}\eta^{ac}\eta^{mn}\partial_{m} \partial_{n} + \frac{1}{2e^{2}} \left[1 - \frac{1}{\xi} \right] \eta^{am} \partial_{m}\partial_{n} \eta^{nc} \right) G_{cb}(z|y) = \delta_{b}^{a} \delta(x - y).
\end{equation}
Again, you go to the Fourier conjugate basis:
\begin{equation}
	G_{ab}(x|y) = \int \int \mathrm{d}k_{x} \mathrm{d}k_{y} \left[ \widehat{G}_{ab}(k_{x}|k_{y}) \exp{\left( -i x \cdot k_{x} + i y \cdot k_{y} \right)} \right].
\end{equation}
Thus,
\begin{equation}
	\left( \eta^{ac} - \left[1 - \frac{1}{\xi} \right] \eta^{am} \frac{k_{m}k_{n}}{k^{2}} \eta^{nc} \right) \widehat{G}_{cb}(k|k_{y}) = \left( \frac{2e^{2}}{k^{2}} \right) \delta_{b}^{a} \delta(k - k_{y}).
\end{equation}
In order to find $\widehat{G}_{ab}$ you need to invert the matrix
\begin{equation}
	M^{ab} = \eta^{ab} - \left[1 - \frac{1}{\xi} \right] \eta^{am} \frac{k_{m}k_{n}}{k^{2}} \eta^{nb}.
\end{equation}
That is, you need a matrix $W_{ab}$ such that
\begin{equation}
	W_{ac} M^{cb} = \delta_{a}^{b}.
\end{equation}
Begin with
\begin{equation}
	W_{ab} = A \eta_{ab} + B \frac{k_{a} k_{b}}{k^{2}}.
\end{equation}
Then it follows that
\begin{equation}
	A \delta_{a}^{b} - A \left[1 - \frac{1}{\xi} \right] \frac{k_{a}k_{n}}{k^{2}} \eta^{nb} + B \frac{k_{a} k_{c}}{k^{2}} \eta^{cb} - B \left[1 - \frac{1}{\xi} \right] \frac{k_{a}k_{n}}{k^{2}} \eta^{nb} = \delta_{a}^{b}.
\end{equation}
From here it follows that
\begin{equation}
	A = 1, \qquad B = \xi - 1.
\end{equation}
Hence,
\begin{equation}
	\widehat{G}_{ab}(k|k_{y}) =  \left( \frac{2 e^{2}}{k^{2}} \eta_{ab} + 2 e^{2}(\xi - 1) \frac{k_{a} k_{b}}{k^{4}} \right) \delta(k - k_{y}),
\end{equation}
and thus
\begin{equation}
	G_{ab}(x|y) = \int \mathrm{d}k \left[ \left( \frac{2 e^{2}}{k^{2}} \eta_{ab} + 2 e^{2}(\xi - 1) \frac{k_{a} k_{b}}{k^{4}} \right) \exp{\left( -i (x - y) \cdot k \right)} \right].
\end{equation}
At this point you could simplify the calculation greatly by working with $\xi = 1$, which is known as the \textbf{Fermi-Feynman gauge}. However, I will keep $\xi$ available for now. The first term in $G_{ab}$ is similar to the massless scalar. For the second term, write
\begin{equation}
	2\frac{k_{a} k_{b}}{k^{4}} = -\frac{1}{2} \lim_{z \rightarrow 0} \left(\frac{\partial}{\partial z^{a}} \frac{\partial}{\partial z^{b}} \right) \int\limits_{0}^{\infty} \mathrm{d}T \left( \frac{1}{T} \right)^{-1} \exp{\left( - \frac{1}{2} k^{2} T -i z \cdot k\right)}
\end{equation}

\begin{equation}
	...
\end{equation}